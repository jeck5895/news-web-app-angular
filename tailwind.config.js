module.exports = {
  theme: {
    extend: {
      spacing: {
        "7": "2rem",
        "8": "2.25rem",
        "9": "2.5rem",
        "10": "2.75rem",
        "11": "3rem",
        "12": "3.25rem",
        "13": "3.5rem",
        "14": "3.75rem",
        "15": "4rem",
        "16": "4.25rem",
        "17": "4.5rem",
        "18": "4.75rem",
        "19": "5rem"
      }
    }
  },
  variants: {},
  plugins: []
};
