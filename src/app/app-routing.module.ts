import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./views/home/home.component";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "news"
  },
  {
    path: "news",
    component: HomeComponent
  },
  /** Lazy loading component in Angular */
  {
    path: "news/:title",
    loadChildren: () =>
      import("./views/details/details.module").then(m => m.DetailsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
