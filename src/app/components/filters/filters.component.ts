import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-filters",
  templateUrl: "./filters.component.html",
  styleUrls: ["./filters.component.scss"]
})
export class FiltersComponent implements OnInit {
  public country: string = "";
  public category: string = "";
  public keyword: string = "";

  constructor() {}

  ngOnInit() {}

  public onSubmit() {}
}
