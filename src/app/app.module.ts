import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CardComponent } from "./components/card/card.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { HomeComponent } from "./views/home/home.component";
import { FiltersComponent } from "./components/filters/filters.component";

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    NavbarComponent,
    HomeComponent,
    FiltersComponent
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
