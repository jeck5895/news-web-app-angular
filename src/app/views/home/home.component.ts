import { Component, OnInit } from "@angular/core";
import { ArrayType } from "@angular/compiler";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  news: Array<Object> = [
    {
      id: 1,
      title: "Sample",
      description: "Lorem Ipsum"
    },
    {
      id: 2,
      title: "Sample",
      description: "Lorem Ipsum"
    },
    {
      id: 3,
      title: "Sample",
      description: "Lorem Ipsum"
    },
    {
      id: 4,
      title: "Sample",
      description: "Lorem Ipsum"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
